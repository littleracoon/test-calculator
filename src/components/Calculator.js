import React, { useState } from 'react';

import './Calculator.css';

function Calculator(props) {
    const [terms, setTerm] = useState({ term1: 0, term2: 0 });
    const [results, setResult] = useState([]);

    const handleSubmit = (event) => {
        event.preventDefault();
        const sum = (+terms.term1) + (+terms.term2)
        setResult([...results, sum])
    }

    return (
        <div className="container">
            <form onSubmit={handleSubmit}>
                <div className="calculator">
                    <div className="calculator-item">
                        <input type="number" value={terms.term1} onChange={event => setTerm({ ...terms, term1: event.target.value })} />
                    </div>
                    <div className="calculator-sign">+</div>
                    <div className="calculator-item">
                        <input type="number" value={terms.term2} onChange={event => setTerm({ ...terms, term2: event.target.value })} />
                    </div>
                    <div className="calculator-sign"><button type="submit">=</button></div>
                    <div className="calculator-answer">{results[results.length-1]}</div>
                </div>
                <div className="calculator-results">{results.join(', ')}</div>
            </form>
        </div>)
}

export default Calculator;